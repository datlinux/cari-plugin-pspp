#!/usr/bin/env bash

set -euo pipefail

TOOL_NAME="pspp"
TOOL_TEST="pspp"
IS_NEW_OR_UPGR="0"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

inst_version() {
  if [ ! -f /usr/bin/pspp ]; then
    return 0
  fi
  echo "`apt-cache policy pspp | head -2 | tail -1 | sed -e 's/ //g' | cut -d ":" -f2 | cut -d "-" -f1`"
}

avail_version() {
  echo "`apt-cache policy pspp | head -3 | tail -1 | sed -e 's/ //g' | cut -d ":" -f2 | cut -d "-" -f1`"
}

list_all_versions() {
  iv=`inst_version`
  av=`avail_version`
  if [[ "${iv}" == "${av}"* ]]; then
    echo "$iv"
  else
    echo "$iv"
    echo "$av"
  fi
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"
  local avail_v="`avail_version`"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    echo "Available version: $avail_v"
    if [ -f "/usr/bin/pspp" ]; then
      local pspp_v="`inst_version`"
      echo "Installed version: $pspp_v"
      if [[ ! "$pspp_v" == "$avail_v" ]]; then
        sudo apt upgrade -y --only-upgrade pspp
      fi
    else
      sudo apt install -y pspp
    fi

    mkdir -p "$install_path/bin"
    touch "$install_path/bin/pspp"
    chmod a+x "$install_path/bin/pspp"
    echo "/usr/bin/psppire \"\$@\"" >> $install_path/bin/pspp
    
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
  if [ -z ${DAT_CUBIC+x} ] && [ -f /usr/share/applications/org.fsf.pspp.desktop ]; then
    sudo rm /usr/share/applications/org.fsf.pspp.desktop
  fi
  exit 0
}
